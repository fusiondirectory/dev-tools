
# FusionDirectory dev tools

This is an ensemble of scripts and tools to help coding plugins for FusionDirectory

## Explanation of the directory structure

 - doxygen : doxygen configuration file that allow to build the documentation of the [stable-api] and [dev-api]
 - locale-scripts : script to generate the locales for FusionDirectory, used in the ci to update the locales
 - php-codesniffer-rules : rules for [PHP_codeSniffer] to be used in ci and development
 - phpstan : rules for the static analyser [phpstan]
 - simple-plugin : simple demo plugin of FusionDirectory API functionalities
 - qa : structure of the test framework used by FusionDirectory along with a small functional test class

## Get help

### Community support

There are a couple of ways you can try [to get help][get help].

### Professional support

Professional support is provided through of subscription.

* [FusionDirectory Subscription][subscription-fusiondirectory] : Global subscription for FusionDirectory 

The subscription provides access to FusionDirectory's enterprise repository, tested and pre-packaged versions with patches between versions, 
providing reliable software updates and security enhancements, as well as technical help and support.

Choose the plan that's right for you. Our subscriptions are flexible and scalable according to your needs

The subscription period is one year from the date of purchase and provides you with access to the extensive infrastructure of enterprise-class software and services.

### Best practice badge

[![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/351/badge)](https://bestpractices.coreinfrastructure.org/projects/351)
  
## Crowfunding

If you like us and want to send us a small contribution, you can use the following crowdfunding services

* [donate-liberapay]

* [donate-kofi]

* [donate-github]
  
## License

[FusionDirectory][FusionDirectory] is  [GPL 2 License](COPYING).

[FusionDirectory]: https://www.fusiondirectory.org/

[fusiondirectory-install]: https://fusiondirectory-user-manual.readthedocs.io/en/latest/fusiondirectory/install/index.html

[get help]: https://fusiondirectory-user-manual.readthedocs.io/en/latest/support/index.html

[subscription-fusiondirectory]: https://www.fusiondirectory.org/en/iam-tool-service-subscriptions/

[register]: https://register.fusiondirectory.org

[donate-liberapay]: https://liberapay.com/fusiondirectory/donate

[donate-kofi]: https://ko-fi.com/fusiondirectory

[donate-github]: https://github.com/fusiondirectory

[stable-api]: https://stable-api.fusiondirectory.org/
[dev-api]: https://dev-api.fusiondirectory.org/
[PHP_codeSniffer]: https://github.com/squizlabs/PHP_CodeSniffer
[phpstan]: https://phpstan.org/

