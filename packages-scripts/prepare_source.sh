#!/bin/sh
# Prepare the sources before we build the packages

# Variables
change_directory=''
distribution=''
package=''
package_version=''
build_version=''

#####################################

# Usage
usage()
{
  echo "Usage: $0 -c change_directory -d distribution -p package -v package_version -b build_version (rpm)"
}

#####################################

# Get options
while getopts "c:d:p:v:b:h" opt; do
  case "$opt" in
    c)
      change_directory=$OPTARG
      ;;
    d)
      distribution=$OPTARG
      ;;
    p)
      package=$OPTARG
      ;;
    v)
      package_version=$OPTARG
      ;;
    b)
      build_version=$OPTARG
      ;;
    h)
      usage
      exit 0
      ;;
    *)
      usage
      exit 1
      ;;
  esac
done

# If not set, exit
if [ ! "$change_directory" ] ; then
  usage
  exit 1
fi

if [ ! "$distribution" ] ; then
  usage
  exit 1
fi

if [ ! "$package" ] ; then
  usage
  exit 1
fi

if [ ! "$package_version" ] ; then
  usage
  exit 1
fi

if [ "$distribution" = 'rpm' ] ; then
  if [ ! "$build_version" ] ; then
    usage
    exit 1
  fi
fi

shift $((OPTIND-1))

if [ $# -gt 0 ] ; then
  cd "$1" || exit 2
fi

#####################################

cd "$change_directory" || exit 2

if [ "$distribution" = 'debian' ] ; then
  # In src directory
  # Rename package-version.tar.gz in package.tar.gz
  mv "$package"-"$package_version".tar.gz "$package".tar.gz || exit 2

  # Create new dir so we can rename it easily
  mkdir tmp
  cd ./tmp/ || exit
  tar -xzf ../"$package".tar.gz || exit 2
  mv ./* ../"$package" || exit 2
  cd .. || exit
  rm -Rf ./tmp/

  # For fusiondirectory package
  if [ "$package" = 'fusiondirectory' ] ; then
    mv "$package" core
    mv "$package"-plugins-"$package_version".tar.gz "$package"-plugins.tar.gz || exit 2
    # Create new dir so we can rename it easily
    mkdir tmp
    cd ./tmp/ || exit
    tar -xzf ../"$package"-plugins.tar.gz || exit 2
    mv ./* ../plugins || exit 2
    cd .. || exit
    rm -Rf ./tmp/
    cp "$package"-plugins.tar.gz ../pkg/"${package}"_"${package_version}".orig-plugins.tar.gz || exit 2
  fi

elif [ "$distribution" = 'rpm' ] ; then
  # Set the good version for tar.gz
  tar xfv "$package"-"$package_version".tar.gz || exit 2
  rm "$package"-"$package_version".tar.gz || exit 2

  if [ "$package_version" != "$build_version" ] ; then
    mv ./"$package"-"$package_version"/ ./"$package"-"$build_version"/ || exit 2
  fi

  tar cf "$package"-"$build_version".tar ./"$package"-"$build_version"/* || exit 2
  gzip "$package"-"$build_version".tar || exit 2
  rm -Rf ./"$package"-"$build_version"/ || exit 2

else
  echo "No actions for $distribution"
  exit 1
fi

