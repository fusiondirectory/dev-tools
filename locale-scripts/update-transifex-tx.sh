#!/bin/bash


if [ -z "$1" ]; then 
   echo "usage: $0 fusiondirectory version\n"
   echo "       ex: FusionDirectory-107\n"
   exit
fi

FD_VERSION=$1

for resource in `ls -1 | grep -v .md`; do
  tx set --auto-local -r $FD_VERSION.$resource $resource/'locale/<lang>/fusiondirectory.po' \
    --source-lang en \
    --source-file $resource'/locale/en/fusiondirectory.po' --execute
done
