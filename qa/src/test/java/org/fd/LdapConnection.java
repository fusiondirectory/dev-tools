package org.fd;

import com.unboundid.ldap.sdk.*;
import com.unboundid.ldif.LDIFException;
import com.unboundid.ldif.LDIFReader;
import com.unboundid.util.Base64;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Consists of all the methods that manipulate the ldap connection
 */
public class LdapConnection {
    private static final Logger LOGGER = Logger.getLogger(LdapConnection.class.getName());

    protected final String ldapAdminConf = "cn=admin,cn=config";
    protected final String ldapPwdConf = "tester";
    protected final String ldapBaseConf = "cn=config";

    public LdapConnection() {
        Utils.customizeLogger(LOGGER);
    }

    /**
     * Inserts the .ldif file given as parameter into a ldap server
     *
     * @param filename the name of the .ldif file
     * @throws IOException if file operations fail
     */
    protected void insertLdif(String filename) throws IOException {
        BufferedReader file = new BufferedReader(new InputStreamReader(new FileInputStream(filename),
                StandardCharsets.UTF_8));
        BufferedWriter tmpFile = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("tmp.ldif"),
                StandardCharsets.UTF_8));
        String line;

        while ((line = file.readLine()) != null) {
            line = line.replaceAll("<LDAP_BASE>", Utils.getLdapBase());
            line = line.replaceAll("<LDAP_HOST>", Utils.getLdapHost());
            line = line.replaceAll("<LDAP_PWD>", Utils.getLdapPwd());
            tmpFile.write(line);
            tmpFile.write("\n");
        }

        file.close();
        tmpFile.close();

        try {
            LDAPConnection connection = new LDAPConnection("localhost",
                    Utils.getPortNumber(), Utils.getLdapAdmin(), Utils.getLdapPwd());
            LDIFReader ldifReader = new LDIFReader("tmp.ldif");

            Entry ldifEntry;
            while ((ldifEntry = ldifReader.readEntry()) != null) {
                AddRequest addRequest = new AddRequest(ldifEntry.getDN(), ldifEntry.getAttributes());
                LDAPResult addResult = connection.add(addRequest);
                LOGGER.info("Added entry: " + ldifEntry.getDN() + ", Result: " + addResult.getResultCode());
            }

            ldifReader.close();
            connection.close();
            boolean deleted = new File("tmp.ldif").delete();
            if (!deleted) {
                LOGGER.log(Level.WARNING, "Tmp file tmp.ldif was not deleted");
            }
        } catch (LDAPException | IOException | LDIFException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * Deletes a ldap entry from the given connection
     *
     * @param connection the connection to the ldap server
     * @param dn the name of dn
     * @param recursive true if recursive is needed, false otherwise
     * @throws LDAPException if the ldap operations fail
     */
    private void deleteLdapEntry(LDAPConnection connection, String dn, boolean recursive) throws LDAPException {
        if (!recursive) {
            connection.delete(dn);
        } else {
            SearchResult searchResult = connection.search(dn, SearchScope.ONE, "(objectclass=*)");
            for (SearchResultEntry entry : searchResult.getSearchEntries()) {
                deleteLdapEntry(connection, entry.getDN(), recursive);
            }
            connection.delete(dn);
        }
    }

    /**
     * Empties the ldap server by deleting all entries
     */
    protected void emptyLdap() {
        try (LDAPConnection connection = new LDAPConnection("localhost", 389,
                Utils.getLdapAdmin(), Utils.getLdapPwd())) {
            SearchResult searchResult = connection.search(Utils.getLdapBase(), SearchScope.ONE, "(objectclass=*)");

            for (SearchResultEntry entry : searchResult.getSearchEntries()) {
                String dn = entry.getDN();
                if (!dn.equals("cn=admin," + Utils.getLdapBase())) {
                    deleteLdapEntry(connection, dn, true);
                }
            }
        } catch (LDAPException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            throw new AssertionError(e.getMessage());
        }
    }

    /**
     * Resets the gosa acl entry
     */
    protected void resetGosaAclEntry() {
        try (LDAPConnection connection = new LDAPConnection("localhost",
                Utils.getPortNumber(), Utils.getLdapAdmin(), Utils.getLdapPwd())) {
            BindResult bindResult = connection.bind(Utils.getLdapAdmin(), Utils.getLdapPwd());

            if (bindResult.getResultCode() == ResultCode.SUCCESS) {
                SearchResult searchResult = connection.search(Utils.getLdapBase(), SearchScope.SUB, "(objectclass=*)");

                if (searchResult.getEntryCount() > 0) {
                    SearchResultEntry entry = searchResult.getSearchEntries().get(0);
                    String gosaAclEntryValue = "0:subtree:" + encodeBase64("cn=admin,ou=aclroles,"
                            + Utils.getLdapBase())
                            + ":" + encodeBase64("uid=fd-admin,ou=people," + Utils.getLdapBase());

                    if (entry.hasAttribute("gosaaclentry")) {
                        ModifyRequest modifyRequest = new ModifyRequest(Utils.getLdapBase(),
                                new Modification(ModificationType.REPLACE, "gosaaclentry", gosaAclEntryValue));
                        connection.modify(modifyRequest);
                        LOGGER.log(Level.INFO, "Successfully replaced gosaAclEntry");
                    } else {
                        ModifyRequest modifyRequest = new ModifyRequest(Utils.getLdapBase(),
                                new Modification(ModificationType.ADD, "gosaaclentry", gosaAclEntryValue));
                        connection.modify(modifyRequest);
                        LOGGER.log(Level.INFO, "Successfully added gosaAclEntry");
                    }
                } else {
                    LOGGER.log(Level.WARNING, "No entry found for the base " + Utils.getLdapBase());
                }
            } else {
                LOGGER.log(Level.SEVERE, "Failed to bind to LDAP with admin credentials");
            }

        } catch (LDAPException e) {
            LOGGER.log(Level.SEVERE, "LDAP Exception: " + e.getMessage());
        }
    }

    /**
     * Checks if the attribute is not set
     *
     * @param attrTag the name of the attribute
     * @return true if the attribute is not set, false otherwise
     */
    protected boolean isOlcAttributeOptionNotSet(String attrTag) {
        try (LDAPConnection connection = new LDAPConnection("localhost",
                Utils.getPortNumber(), ldapAdminConf, ldapPwdConf)) {
            SearchResult searchResult = connection.search(ldapBaseConf, SearchScope.SUB, "(olcAttributeOptions=*)");

            if (searchResult.getEntryCount() > 0) {
                SearchResultEntry entry = searchResult.getSearchEntries().get(0);
                Attribute attribute = entry.getAttribute("olcAttributeOptions");

                if (attribute != null) {
                    for (String value : attribute.getValues()) {
                        if (value.equals(attrTag)) {
                            return false;
                        }
                    }
                }
            }
            return true;

        } catch (LDAPException e) {
            LOGGER.log(Level.SEVERE, "Error in LDAP operation: " + e.getMessage());
            return true;
        }
    }

    /**
     * Activates an attribute
     *
     * @param attrTag the name of the attribute
     * @param controls extra controls to be included
     * @return true if the update is successful, false otherwise
     */
    protected boolean activateOlcAttributeOptions(String attrTag, Control... controls) {
        try (LDAPConnection connection = new LDAPConnection("localhost",
                Utils.getPortNumber(), ldapAdminConf, ldapPwdConf)) {

            if (isOlcAttributeOptionNotSet(attrTag)) {
                Modification modification = new Modification(ModificationType.ADD, "olcAttributeOptions", attrTag);
                ModifyRequest request = new ModifyRequest(ldapBaseConf, modification, controls);
                LDAPResult result = connection.modify(request);
                return result.getResultCode() == ResultCode.SUCCESS;
            }

            return false;

        } catch (LDAPException e) {
            LOGGER.log(Level.SEVERE, "Error in LDAP operation: " + e.getMessage());
            return false;
        }
    }

    /**
     * Encodes a string with the base64 encoding
     *
     * @param input the input to be encoded
     * @return the encoded string
     */
    private static String encodeBase64(String input) {
        return Base64.encode(input.getBytes(StandardCharsets.UTF_8));
    }
}
