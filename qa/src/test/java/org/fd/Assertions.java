package org.fd;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Consists of all the methods that assert something
 */
public class Assertions {
    private static final Logger LOGGER = Logger.getLogger(Assertions.class.getName());
    private final WebDriver driver;
    private final WebDriverWait wait;

    /**
     * Constructs an Assertions instance
     *
     * @param driver      the driver where the tests run
     * @param wait        the waiter of the driver
     * @param downloadDir the directory for downloading files
     * @param vnuJar      the path to the vnu jar
     */
    public Assertions(WebDriver driver, WebDriverWait wait, String downloadDir, String vnuJar) {
        this.driver = driver;
        this.wait = wait;
        Utils.customizeLogger(LOGGER);
    }

    /**
     * Checks if the user with the given username is logged in
     *
     * @param username the username of the desired logged in user
     * @throws AssertionError when an unexpected user is logged in
     */
    public void assertLoggedIn(String username) {
        wait.until(ExpectedConditions.urlContains("/fusiondirectory/main.php"));
        if (username != null) {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("header-right")));
            WebElement element = driver.findElement(By.id("header-right"))
                    .findElement(By.tagName("a"))
                    .findElement(By.tagName("b"));
            if (!username.equals(element.getText())) {
                throw new AssertionError("Expected user: " + username + ", but found: " + element.getText());
            }
        }
    }

    /**
     * Checks if the login has failed
     */
    public void assertLoginFailed() {
        String url = driver.getCurrentUrl();
        assertTrue(url.contains("/fusiondirectory/index.php"));
        WebElement element = driver.findElement(By.id("window-footer"));
        assertEquals("Invalid credentials", element.getText());
    }
}
