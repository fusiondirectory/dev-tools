package org.fd;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestWatcher;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.*;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Consists of all the methods that define actions when test is finished
 */
public class ScreenshotTestWatcher implements TestWatcher {
    private static final Logger LOGGER = Logger.getLogger(ScreenshotTestWatcher.class.getName());

    public ScreenshotTestWatcher() {
        Utils.customizeLogger(LOGGER);
    }

    /**
     * Copies the log file to a txt file in the screenshotDir
     *
     * @param methodName the name of the method that triggered the copy log
     */
    private void copyLogFile(String methodName) {
        Path sourcePath = Paths.get(Utils.getApacheLogFile());
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss");
        LocalDateTime now = LocalDateTime.now();
        String timestamp = dtf.format(now);
        Path destinationPath = Paths.get(Utils.getScreenshotDir() + timestamp + "_" + methodName + ".apacheLogs.txt");

        try {
            Files.copy(sourcePath, destinationPath, StandardCopyOption.REPLACE_EXISTING);
            LOGGER.log(Level.INFO, "Log file copied successfully at " + destinationPath);
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Log file could not e copied because " + e.getMessage());
        }

        destinationPath = Paths.get(Utils.getScreenshotDir() + timestamp + "_" + methodName + ".testLogs.txt");
        try {
            Files.copy(Paths.get(Utils.getLogPath() + "tests.log"), destinationPath,
                    StandardCopyOption.REPLACE_EXISTING);
            LOGGER.log(Level.INFO, "Log file copied successfully at " + destinationPath);
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Log file could not e copied because " + e.getMessage());
        }
    }

    /**
     * Takes a screenshot of the driver and saves it as a .png file
     *
     * @param methodName the name of the method that triggered the screenshot
     * @param driver the WebDriver for which to take screenshot
     */
    private void captureScreenshot(String methodName, WebDriver driver) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss");
        LocalDateTime now = LocalDateTime.now();
        String timestamp = dtf.format(now);
        String screenshotFileName = methodName + "_" + timestamp + ".png";
        File screenshotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

        Path screenshotPath = Paths.get(Utils.getScreenshotDir() + screenshotFileName);

        try {
            Files.move(screenshotFile.toPath(), screenshotPath);
            LOGGER.info("Screenshot saved: " + screenshotPath);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Failed to save screenshot: " + e.getMessage());
        }
    }

    /**
     * Extract the driver from the testInstance passed as parameter at runtime
     *
     * @param testInstance the instance of the running test
     * @return the driver field as WebDriver object
     */
    private static WebDriver getDriver(FusionDirectoryTestCase testInstance) {
        try {
            Field field = FusionDirectoryTestCase.class.getDeclaredField("driver");
            field.setAccessible(true);
            return (WebDriver) field.get(testInstance);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            LOGGER.log(Level.WARNING, "Driver field was not retrieved correctly");
            return null;
        }
    }

    /**
     * Quits the driver if the test is successful or aborted
     *
     * @param context the context of the test case
     */
    private static void quitDriver(ExtensionContext context) {
        Object testInstance = context.getTestInstance().orElse(null);

        if (testInstance instanceof FusionDirectoryTestCase) {
            WebDriver driver = getDriver((FusionDirectoryTestCase) testInstance);
            quitDriver(driver);
        }
    }

    /**
     * Quits the driver passed as parameter
     *
     * @param driver the driver to be quited
     */
    private static void quitDriver(WebDriver driver) {
        if (driver != null) {
            driver.quit();
            LOGGER.log(Level.INFO, "Driver was quited successfully");
        } else {
            LOGGER.log(Level.WARNING, "Driver was null, could not quit");
        }
    }

    /**
     * Defines the actions to do when a test is failing
     *
     * @param context the current extension context; never {@code null}
     * @param cause the throwable that caused test failure; may be {@code null}
     */
    @Override
    public void testFailed(ExtensionContext context, Throwable cause) {
        LOGGER.log(Level.SEVERE, cause.getMessage());
        Object testInstance = context.getTestInstance().orElse(null);

        if (testInstance instanceof FusionDirectoryTestCase) {
            WebDriver driver = getDriver((FusionDirectoryTestCase) testInstance);
            captureScreenshot(context.getDisplayName(), driver);
            quitDriver(driver);
            copyLogFile(context.getDisplayName());
        }
    }

    /**
     * Defines the actions to be done when a test is successful
     *
     * @param context the current extension context; never {@code null}
     */
    @Override
    public void testSuccessful(ExtensionContext context) {
        quitDriver(context);
    }

    /**
     * Defines the actions to be done when a test is aborted
     *
     * @param context the current extension context; never {@code null}
     * @param cause the throwable responsible for the test being aborted; may be {@code null}
     */
    @Override
    public void testAborted(ExtensionContext context, Throwable cause) {
        quitDriver(context);
    }
}
