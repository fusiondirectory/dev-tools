package org.fd;

import java.io.*;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Includes methods that are useful for multiple classes
 */
public class Utils {
    private static final Logger LOGGER = Logger.getLogger(Utils.class.getName());
    private static String ldapBase;
    private static String ldapAdmin;
    private static String ldapPwd;
    private static String ldapHost;
    private static int portNumber;
    private static String vnuJar;
    private static String apacheLogFile;
    private static String seleniumHost;
    private static String fdHost;
    private static boolean headless;
    private static String downloadDir;
    private static String hookDirectory;
    private static String screenshotDir = "/var/www/html/";
    private static String logPath = "/var/log/";
    private static FileHandler fileHandler;

    static {
        try {
            fileHandler = new FileHandler(logPath + "tests.log", true);
            fileHandler.setFormatter(new SimpleFormatter());
        } catch (IOException e) {
            LOGGER.severe("Failed to initialize log file handler: " + e.getMessage());
        }
    }

    /**
     * Reads the ini config file and updates fields
     *
     * @throws RuntimeException when the config file is not found
     */
    public void readConfig() {
        customizeLogger(LOGGER);
        Properties properties = new Properties();
        String configFile = Objects.requireNonNull(getClass().getClassLoader().getResource("testsConfig.ini"))
                .getPath();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(configFile),
                StandardCharsets.UTF_8))) {
            properties.load(reader);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Config file was not found");
            throw new RuntimeException("Config file was not found");
        }
        properties.stringPropertyNames().forEach(property -> {
            try {
                Field field = findField(property);
                if (field == null) {
                    LOGGER.log(Level.WARNING, "Property " + property + " was not found");
                    return;
                }
                field.setAccessible(true);
                if (field.getType().equals(String.class)) {
                    field.set(this, properties.getProperty(property));
                }
                if (field.getType().equals(boolean.class)) {
                    boolean value = Boolean.parseBoolean(properties.getProperty(property));
                    field.setBoolean(this, value);
                }
                LOGGER.log(Level.INFO, "Set from config file " + field.getName() + "="
                        + properties.getProperty(property));
            } catch (Exception e) {
                LOGGER.log(Level.WARNING, "Could not treat key " + property + "(exception: " + e.getMessage() + ")");
            }
        });
    }

    /**
     * Finds a field by field name in the tree structure of this class
     *
     * @param fieldName the name of the field to be found
     * @return the field object with the given name
     */
    private static Field findField(String fieldName) {
        Class<?> clazz = Utils.class;
        while (clazz != null) {
            try {
                return clazz.getDeclaredField(fieldName);
            } catch (NoSuchFieldException e) {
                clazz = clazz.getSuperclass();
            }
        }

        return null;
    }

    public static String getLdapBase() {
        return ldapBase;
    }

    public static String getLdapAdmin() {
        return ldapAdmin;
    }

    public static String getLdapPwd() {
        return ldapPwd;
    }

    public static String getLdapHost() {
        return ldapHost;
    }

    public static int getPortNumber() {
        return portNumber;
    }

    public static String getVnuJar() {
        return vnuJar;
    }

    public static String getApacheLogFile() {
        return apacheLogFile;
    }

    public static String getSeleniumHost() {
        return seleniumHost;
    }

    public static String getFdHost() {
        return fdHost;
    }

    public static boolean isHeadless() {
        return headless;
    }

    public static String getDownloadDir() {
        return downloadDir;
    }

    public static String getHookDirectory() {
        return hookDirectory;
    }

    public static String getScreenshotDir() {
        return screenshotDir;
    }

    public static String getLogPath() {
        return logPath;
    }

    public static void setPortNumber(int portNumber) {
        Utils.portNumber = portNumber;
    }

    /**
     * Adds a handler to the logger to write logs in file
     *
     * @param logger the logger to be customized
     */
    public static void customizeLogger(Logger logger) {
        if (fileHandler != null && logger.getHandlers().length == 0) {
            logger.addHandler(fileHandler);
        }
        logger.setUseParentHandlers(false);
    }
}
