package org.fd.tests;

import org.fd.FusionDirectoryTestCase;
import org.junit.jupiter.api.Test;

public class LoginTest extends FusionDirectoryTestCase {
    public LoginTest() {
        initLdifs = new String[]{"default.ldif"};
    }

    @Test
    public void testGoodLogin() {
        login("fd-admin", "adminpwd");
        getAssertions().assertLoggedIn("fd-admin");
    }

    @Test
    public void testBadLogin() {
        login("fd-admin", "wrongpwd");
        getAssertions().assertLoginFailed();
    }

    @Test
    public void testBadLogin2() {
        login("fdadmin", "adminpwd");
        getAssertions().assertLoginFailed();
    }
}
