package org.fd;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.*;
import java.net.URL;
import java.time.Duration;
import java.util.*;
import java.util.logging.Logger;

/**
 * Represents the basic template of a test class
 */
@ExtendWith(ScreenshotTestWatcher.class)
public class FusionDirectoryTestCase extends LdapConnection {
    private static final Logger LOGGER = Logger.getLogger(FusionDirectoryTestCase.class.getName());
    private static WebDriver driver;
    protected String[] initLdifs;
    private WebDriverWait wait;
    private Assertions assertions;

    /**
     * Defines the initial setup of the tests
     */
    @BeforeAll
    public static void initialSetUp() {
        new Utils().readConfig();
    }

    /**
     * Does the initial setup before each test
     *
     * @throws IOException if insertLDIF method fails or if host cannot be converted to URL
     */
    @BeforeEach
    protected void setUp() throws IOException {
        new FileOutputStream(Utils.getApacheLogFile()).close();
        new FileOutputStream(Utils.getLogPath() + "tests.log").close();
        Utils.customizeLogger(LOGGER);
        Utils.setPortNumber(Integer.parseInt(Utils.getLdapHost().substring(Utils.getLdapHost().indexOf(":") + 1)));
        emptyLdap();
        resetGosaAclEntry();
        activateOlcAttributeOptions("x-");

        for (String ldifFile : initLdifs) {
            insertLdif(Objects.requireNonNull(getClass().getClassLoader().getResource("ldifs/" + ldifFile)).getPath());
        }

        initDriver();
        assertions = new Assertions(driver, wait, Utils.getDownloadDir(), Utils.getVnuJar());
    }

    protected void initDriver() throws IOException {
        FirefoxOptions firefoxOptions = getFirefoxOptions();
        driver = new RemoteWebDriver(new URL(Utils.getSeleniumHost()), firefoxOptions);
        wait = new WebDriverWait(driver, Duration.ofSeconds(5));
    }

    /**
     * Constructs the firefox options for the driver
     *
     * @return the Firefox options for the driver
     */
    private FirefoxOptions getFirefoxOptions() {
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        if (Utils.isHeadless()) {
            firefoxOptions.addArguments("--headless");
        }

        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.dir", Utils.getDownloadDir());
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/octet-stream, text/x-csv");
        profile.setPreference("browser.helperApps.alwaysAsk.force", false);

        firefoxOptions.setProfile(profile);
        return firefoxOptions;
    }

    /**
     * Provides access to the methods inside the assertions object
     *
     * @return the assertions object
     */
    public Assertions getAssertions() {
        return assertions;
    }

    /**
     * Checks if the values from the fields are the same as the passed arguments
     *
     * @param username the username of the user
     * @param password the password of the user
     */
    private void verifyIdFields(String username, String password) {
        WebElement usernameField = driver.findElement(By.id("username"));
        WebElement passwordField = driver.findElement(By.id("password"));

        if (!usernameField.getAttribute("value").equals(username)) {
            throw new RuntimeException("Username field was not filled correctly");
        }
        if (!passwordField.getAttribute("value").equals(password)) {
            throw new RuntimeException("Password field was not filled correctly");
        }
    }

    /**
     * On the web interface, fills the correct fields with the given values
     *
     * @param fields map of string to string, where key is the name of field and value is the value desired for field
     */
    protected void fillFields(Map<String, String> fields) {
        for (Map.Entry<String, String> entry : fields.entrySet()) {
            if (entry.getValue() != null) {
                WebElement element = driver.findElement(By.id(entry.getKey()));
                element.clear();
                element.sendKeys(entry.getValue());
            }
        }
    }

    /**
     * Attempts logging in the user with the given credentials
     *
     * @param username the username of the login user
     * @param password the password of the login user
     */
    protected void login(String username, String password) {
        driver.get("http://" + Utils.getFdHost() + "/fusiondirectory/index.php");

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("password")));

        Map<String, String> fields = new HashMap<>();
        fields.put("username", username);
        fields.put("password", password);
        fillFields(fields);

        verifyIdFields(username, password);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("login")));

        WebElement loginButton = driver.findElement(By.name("login"));
        loginButton.click();
    }
}
