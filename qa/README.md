## General
For the whole structure of the project gradle is used. This helps for importing the necessary dependencies automatically and to run the tests with just one command.
When running the tests you might want to log out from fusion directory browser to not interfere with the tests.
When running the tests you need to edit the `qa/src/resources/testsConfig.ini` file with the according values (do not change the keys, they will not work otherwise). All keys are needed, otherwise the tests will not work because the fields have no default values (except for screenshotDir).
Before running the tests start the selenium server (I used the 4.23.1 version). The version can be downloaded from https://www.selenium.dev/downloads/. Run it with `java -jar ./selenium-server-4.23.1.jar standalone`. In order for the tests to run you need to have java 17 installed on your machine. Use this link for how to install it https://computingforgeeks.com/install-oracle-java-openjdk-on-debian-linux/. Make sure you have version 0.35 of geckodriver installed. For the Orchestrator tests make sure to have `tester` + `tester` in the DSA part (this is how the tests are configured to work).

### Gradle commands
We are using a plugin (defined in `build.gradle` file in `plugins` section). This plugin allows us a nicer show of the test results. The default version that we are using is showing the tests that have passed along with the time they took. This plugin can be customized as documented in https://github.com/radarsh/gradle-test-logger-plugin. <br />
All commands must be run from inside the automated-testing/qa folder
- `./gradlew clean` command cleans the gradle folder (usually use this before running tests - not necessary tho)
- `./gradlew test` command runs all the tests inside the project (meaning all methods annotated with @Test)
- `./gradlew test --tests "path to test class"` will run a specific test class (the path will begin with org.fd.tests.)
- `./gradlew test --tests "path to test class.test method name"` will run a specific test method (the path is the same as before and test method name is the name of the method from that class that you want to run)

## Project structure
All testing code and testing logic is inside the `qa/` folder
`qa.iml` file is needed for IntelliJ to structure the project nicely when opening it. It is not used for anything else except this.

### qa/ folder
`build.gradle` file has all the dependencies of the project written in gradle format (if any additional jar is needed the format is usually found on maven repository https://mvnrepository.com) <br />
`gradlew` (for linux/ macOS) and `gradlew.bat` (for Windows) files are the scripts that actually runs for the gradle commands <br />
`settings.gradle` file is defining the name of the root project (in this case qa) <br />
`gradle/` folder contains the  gradle jar <br />
`src/` folder contains the core of the tests
- `main/` folder is empty because the backend is not written here
- `test/` folder contains a `java/org/fd` folder and a `resources` folder
- `test/resources` folder contains al the necessary extra files to run the tests (eg: the ldif files for ldap, the ini file for configuration)
- `test/java` folder has all the java files for the tests
- `Assertions.java` file contains all the methods that assert something (their names start with assert)
- `LdapConnection.java` file contains all the methods that are used to configure the ldap connection (eg: inserting the ldif files, emptying the ldap)
- `FusionDirectoryTestCase.java` file represents the big template of writing a basic test, this includes all methods related to access the web interface (eg: clicking buttons, filling in values)
- `ScreenshotTestWatcher.java` file is an additional file used for defining what will happen when the test fails (we are taking a screenshot), succeeds and aborts (just quit the driver)
- `test/java/org/fd/tests/` folder has all the test classes in the project (this can be structured more by having `core`/`plugins` folders)

### qa/config folder
- this folder contains 2 .xml files that are used by the gitlab-ci.yml file to run the checkstyle pipeline for the whole project
- `checkstyle.xml` file defines all the rules that the pipeline will look for (they check for correct indentation, correct order of methods, java docs, etc)
- `suppressions.xml` file defines all the exceptions to the rules (here we defined that Test classes and methods do not need java docs)/